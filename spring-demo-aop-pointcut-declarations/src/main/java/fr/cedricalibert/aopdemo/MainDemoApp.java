package fr.cedricalibert.aopdemo;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import fr.cedricalibert.aopdemo.dao.AccountDAO;
import fr.cedricalibert.aopdemo.dao.MembershipDAO;

public class MainDemoApp {

	public static void main(String[] args) {
		// read Spring config java class
		AnnotationConfigApplicationContext context = 
				new AnnotationConfigApplicationContext(DemoConfig.class);
		
		
		//get the bean from spring container
		AccountDAO accountDAO = context.getBean("accountDAO", AccountDAO.class);
		
		// get membership bean
		MembershipDAO membershipDAO = context.getBean("membershipDAO",MembershipDAO.class);
		
		//call the businness method
		Account account = new Account();
		accountDAO.addAccount(account, true);
		accountDAO.doWork();
		
		//call setter and getter
		accountDAO.setServiceCode("test");
		accountDAO.setName("testName");
		
		String name = accountDAO.getName();
		String serviceCode = accountDAO.getServiceCode();
		
		//call the method from mebership
		membershipDAO.addSillyMember();
		membershipDAO.goToSleep();
		
		//close spring context
		context.close();

	}

}
