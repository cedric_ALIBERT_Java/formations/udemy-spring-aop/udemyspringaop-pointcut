package fr.cedricalibert.aopdemo.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class MyDemoLoggingAspect {
	
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.*(..))")
	private void forDaoPackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.get*(..))")
	private void forGettersDaoPackage() {}
	
	@Pointcut("execution(* fr.cedricalibert.aopdemo.dao.*.set*(..))")
	private void forSettersDaoPackage() {}
	
	@Pointcut("forDaoPackage() && !(forGettersDaoPackage() || forSettersDaoPackage())")
	private void forDaoPackageExceptSettersAndGetters() {}
	
	
	@Before("forDaoPackageExceptSettersAndGetters()")
	public void beforeAddAccountAdvice() {
		System.out.println("\n======> Executing @Before advice on addAccount");
	}
	
	@Before("forDaoPackageExceptSettersAndGetters()")
	public void performAPIAnalytics() {
		System.out.println("\n======> Perform API analytics");
	}
	
}
